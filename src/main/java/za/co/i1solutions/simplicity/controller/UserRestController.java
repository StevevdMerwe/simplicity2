package za.co.i1solutions.simplicity.controller;

import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.i1solutions.simplicity.domain.User;
import za.co.i1solutions.simplicity.repositories.UserRepository;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("users")
public class UserRestController {

    private final UserRepository userRepository;

    @Autowired
    public UserRestController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @GetMapping(value = "/getUsers", produces = "application/json")
    public Map<String,List<User>> all() {
        Iterable it = userRepository.findAll();
        Iterator<User> myIterator = it.iterator();

        List<User> myList = IteratorUtils.toList(myIterator);
        Map<String,List<User>> map = new HashMap<>();
        map.put("data",myList);
        return map;
    }

}
