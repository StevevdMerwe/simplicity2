package za.co.i1solutions.simplicity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableJpaRepositories(basePackages="za.co.i1solutions.simplicity.repositories")
@EnableTransactionManagement
@EntityScan(basePackages="za.co.i1solutions.simplicity.domain")

public class SimplicityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimplicityApplication.class, args);
    }

}
