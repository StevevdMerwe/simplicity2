package za.co.i1solutions.simplicity.domain;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

@Service
public class ObjectWithSomeProperties {

    public Map<String, Object> convertToArray(Object object) {

        Map ret = new HashMap<String, Object>();
        // Load all fields in the class (private included)
        Field[] attributes = object.getClass().getDeclaredFields();

        for (Field field : attributes) {
            // Dynamically read Attribute Name
            try {
                ret.put(field.getName(), PropertyUtils.getProperty(object, field.getName()));
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }
}