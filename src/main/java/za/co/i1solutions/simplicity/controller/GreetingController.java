package za.co.i1solutions.simplicity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import za.co.i1solutions.simplicity.domain.Greeting;
import za.co.i1solutions.simplicity.domain.ObjectWithSomeProperties;

@Controller
public class GreetingController {

    private static final String DASHBOARD = "dashboard";
    private static final String TABLES = "tables";
    private static final String NAV_DASH = "nav_dash";
    private static final String NAV_TABLES = "nav_tables";

    @Autowired
    ObjectWithSomeProperties objectWithSomeProperties;

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute(NAV_DASH,"active");
        return DASHBOARD;
    }

    @GetMapping("/greeting")
    public String greetingForm(Model model) {
        Greeting greet = new Greeting(1L,"Hello");
        model.addAttribute("greeting", greet);
        model.addAttribute(NAV_DASH,"active");
        return DASHBOARD;
    }

    @GetMapping("/tables")
    public String tables(Model model) {
        model.addAttribute(NAV_TABLES,"active");
        return TABLES;
    }

}
